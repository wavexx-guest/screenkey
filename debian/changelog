screenkey (1:1.4-1) unstable; urgency=medium

  * new upstream version
  * upgraded Standards-Version: 4.5.1
  * updated debian patch
  * modified the rule override_dh_clean: mo files are deleted under
    Screenkey/locale/*/LC_MESSAGES/

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 28 Jan 2021 19:51:46 +0100

screenkey (1:1.3-1) unstable; urgency=medium

  * new upstream version

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 22 Dec 2020 17:22:45 +0100

screenkey (1:1.2-2) unstable; urgency=medium

  * use Ayatana AppIndicator. Closes: #977156
  * upgraded : debhelper-compat (= 13), Standards-Version: 4.5.0
  * modified the dependency:  gir1.2-appindicator3-0.1 =>
    gir1.2-ayatanaappindicator3-0.1

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 21 Dec 2020 12:16:10 +0100

screenkey (1:1.2-1) unstable; urgency=medium

  * New upstream version 1.2

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 15 Aug 2020 19:12:43 +0200

screenkey (1:1.1-2) unstable; urgency=medium

  * just a release to upload a source-only package

 -- Georges Khaznadar <georgesk@debian.org>  Thu, 11 Jun 2020 18:30:32 +0200

screenkey (1:1.1-1) unstable; urgency=medium

  * New upstream version 1.1: Closes: #962504 and Closes: #962118
  * added new dependencies: gir1.2-gtk-3.0, gir1.2-appindicator3-0.1,
    fonts-font-awesome, slop

 -- Georges Khaznadar <georgesk@debian.org>  Wed, 10 Jun 2020 10:13:21 +0200

screenkey (1:0.10~rc1-1) unstable; urgency=medium

  * New upstream version 0.10~rc1, from https://gitlab.com/screenkey/screenkey
  * Updated the file d/watch
  * Changed the epoch of the package
  * Now Python3 is supported "officially", and the issue with settings
    seems to be be cleared. Closes: #959475

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 11 May 2020 17:21:31 +0200

screenkey (0.10-1) unstable; urgency=medium

  * New upstream release
  * upgraded d/control to use Python3, dh >= 11, etc.
    the build-dependency on pygtk is replaced by python3-gi
    Closes: #938450, Closes: #939111, Closes: #885478, Closes: #910116
  * Updated the home page, Closes: #940798
  * removed useless debian patches

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 02 Nov 2019 16:49:22 +0100

screenkey (0.9-3) unstable; urgency=medium

  * removed dependencies on x11-xserver-utils and python-xlib, added
    a dependency on libx11-6, as hinted by Yuri D'Elia
  * updated Standards-Version: 4.1.3, and debhelper level to 10

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 13 Jan 2018 12:21:05 +0100

screenkey (0.9-2) unstable; urgency=medium

  * enriched the French translation po/fr.po
  * created a file po/Makefile, and applied it, so other .po files
    got new untranslated messages.
  * updated shortly the manual page

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 29 Oct 2017 21:31:06 +0100

screenkey (0.9-1) unstable; urgency=medium

  * created a d/watch file to track https://github.com/wavexx/screenkey.
    Closes: #800514; Closes: #782006; Closes: #749029
  * upgraded to the newest version
  * updated Standards-Version and debhelper minimal version
  * added a build-dependency on python-distutils-extra, and an override for
    dh_clean, since the clean by python scripts adds an .eggs directory
  * added a build-dependency on python-setuptools
  * fixed my e-mail address in d/control
  * removed a Python package dependency defined in setup.py

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 29 Oct 2017 20:49:04 +0100

screenkey (0.2-3) unstable; urgency=medium

  * changed my DEBEMAIL georgesk@ofset.org => georgesk@debian.org

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 24 Apr 2017 16:34:51 +0200

screenkey (0.2-2) unstable; urgency=low

  * added dependencies for python-gtk2 and x11-xserver-utils.
    Closes: #684651

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 17 Aug 2012 18:19:29 +0200

screenkey (0.2-1) unstable; urgency=low

  * Initial release (Closes: #681514)

 -- Georges Khaznadar <georgesk@ofset.org>  Fri, 13 Jul 2012 21:56:16 +0200
