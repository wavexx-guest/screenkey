#!/bin/sh

version=$2

cd ..
tar xzf screenkey_${version}.orig.tar.gz
mv screenkey-screenkey-${version} screenkey-${version}

rm -f $(readlink screenkey_${version}.orig.tar.gz)
rm -f screenkey_${version}.orig.tar.gz

tar cJf screenkey_${version}.orig.tar.xz screenkey-${version}

echo "created screenkey_${version}.orig.tar.xz and screenkey-${version}"
